﻿Imports Counter

Public Class BasicCounter
    Private Sub Btn_moins_Click(sender As Object, e As EventArgs) Handles Btn_moins.Click
        Lbl_Counter.Text = CStr(Counter.BasicCounter.Decrementation())
    End Sub

    Private Sub Btn_plus_Click(sender As Object, e As EventArgs) Handles Btn_plus.Click
        Lbl_Counter.Text = CStr(Counter.BasicCounter.Incrementation())
    End Sub

    Private Sub Btn_raz_Click(sender As Object, e As EventArgs) Handles Btn_raz.Click
        Lbl_Counter.Text = CStr(Counter.BasicCounter.Raz())
    End Sub
End Class
