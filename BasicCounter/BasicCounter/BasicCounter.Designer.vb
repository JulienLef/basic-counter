﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BasicCounter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Lbl_Counter = New System.Windows.Forms.Label()
        Me.Lbl_Total = New System.Windows.Forms.Label()
        Me.Btn_plus = New System.Windows.Forms.Button()
        Me.Btn_moins = New System.Windows.Forms.Button()
        Me.Btn_raz = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Lbl_Counter
        '
        Me.Lbl_Counter.AutoSize = True
        Me.Lbl_Counter.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold)
        Me.Lbl_Counter.Location = New System.Drawing.Point(373, 124)
        Me.Lbl_Counter.Name = "Lbl_Counter"
        Me.Lbl_Counter.Size = New System.Drawing.Size(37, 39)
        Me.Lbl_Counter.TabIndex = 0
        Me.Lbl_Counter.Text = "0"
        '
        'Lbl_Total
        '
        Me.Lbl_Total.AutoSize = True
        Me.Lbl_Total.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.Lbl_Total.Location = New System.Drawing.Point(361, 89)
        Me.Lbl_Total.Name = "Lbl_Total"
        Me.Lbl_Total.Size = New System.Drawing.Size(59, 26)
        Me.Lbl_Total.TabIndex = 1
        Me.Lbl_Total.Text = "Total"
        '
        'Btn_plus
        '
        Me.Btn_plus.Location = New System.Drawing.Point(451, 124)
        Me.Btn_plus.Name = "Btn_plus"
        Me.Btn_plus.Size = New System.Drawing.Size(114, 52)
        Me.Btn_plus.TabIndex = 2
        Me.Btn_plus.Text = "+"
        Me.Btn_plus.UseVisualStyleBackColor = True
        '
        'Btn_moins
        '
        Me.Btn_moins.Location = New System.Drawing.Point(211, 124)
        Me.Btn_moins.Name = "Btn_moins"
        Me.Btn_moins.Size = New System.Drawing.Size(114, 52)
        Me.Btn_moins.TabIndex = 3
        Me.Btn_moins.Text = "-"
        Me.Btn_moins.UseVisualStyleBackColor = True
        '
        'Btn_raz
        '
        Me.Btn_raz.Location = New System.Drawing.Point(336, 217)
        Me.Btn_raz.Name = "Btn_raz"
        Me.Btn_raz.Size = New System.Drawing.Size(114, 52)
        Me.Btn_raz.TabIndex = 4
        Me.Btn_raz.Text = "RAZ"
        Me.Btn_raz.UseVisualStyleBackColor = True
        '
        'BasicCounter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Btn_raz)
        Me.Controls.Add(Me.Btn_moins)
        Me.Controls.Add(Me.Btn_plus)
        Me.Controls.Add(Me.Lbl_Total)
        Me.Controls.Add(Me.Lbl_Counter)
        Me.Name = "BasicCounter"
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Lbl_Counter As Label
    Friend WithEvents Lbl_Total As Label
    Friend WithEvents Btn_plus As Button
    Friend WithEvents Btn_moins As Button
    Friend WithEvents Btn_raz As Button
End Class
