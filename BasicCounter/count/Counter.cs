﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Counter
{
    public class BasicCounter
    {
        private static int Valeur;

        public BasicCounter(int number)
        {
            BasicCounter.Valeur = number;
        }
        public static int Incrementation()
        {
            Valeur += 1;
            return Valeur;
        }
        public static int Decrementation()
        {
            Valeur -= 1;
            if (Valeur < 0)
                Valeur = 0;
            return Valeur;
        }
        public static int Raz()
        {
            Valeur = 0;
                return Valeur;
        }
    }
}
